## Services
 - [Info](#Info)
 - [Startup](#Startup)
 - [Public](#Public)
 - [Flight](#Flight)
 - [Hotel](#Hotel)
 - [HotelFlight](#HotelFlight)
 - [Package](#Package)
 - [Insurance](#Insurance)
 - [StaticPage](#StaticPage)
 - [Login](#Login)

<h3><a name="Info"></a>Info</h3>

 Total Runs | Total Api Calls(Last Test) | Total Size(Last Test in KB) | Total Wait Time(Last Test in Min)
--- | --- | --- | ---
1201 | 2 | 3 | 0

<h3><a name="Startup"></a>Startup</h3>

 Name | Message | Wait Time(Sec)  |  Size(KB)  |  Status Code  | Total Calls | Status | Issues
--- | --- | --- | --- | --- | --- | --- | ---
Common/StaticDataService.svc/MobileAppStartupService | OK | 1 | 1 | 200 | 1065 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/3)

<h3><a name="Public"></a>Public</h3>

 Name | Message | Wait Time(Sec)  |  Size(KB)  |  Status Code  | Total Calls | Status | Issues
--- | --- | --- | --- | --- | --- | --- | ---
Common/StaticDataService.svc/PurchaseService | OK | 4 | 1 | 200 | 1382 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Common/StaticDataService.svc/GetCountryAjaxWithCulture | OK | 1 | 1 | 200 | 434 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Common/StaticDataService.svc/GetPreFactorDetails | OK | 1 | 3 | 200 | 1813 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)

<h3><a name="Flight"></a>Flight</h3>

 Name | Message | Wait Time(Sec)  |  Size(KB)  |  Status Code  | Total Calls | Status | Issues
--- | --- | --- | --- | --- | --- | --- | ---
Common/StaticDataService.svc/GetAirportWithParentsWithCulture | OK | 1 | 1 | 200 | 1961 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Flight/FlightService.svc/SearchFlights | OK | 8 | 1114 | 200 | 911 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Common/StaticDataService.svc/GetIsDomestic | OK | 1 | 1 | 200 | 1460 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Flight/FlightService.svc/PurchaseFlight | OK | 8 | 8 | 200 | 904 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
HotelFlight/HotelFlightService.svc/HotelPlusFlightChangeFlt | OK | 1 | 1085 | 200 | 545 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)

<h3><a name="Hotel"></a>Hotel</h3>

 Name | Message | Wait Time(Sec)  |  Size(KB)  |  Status Code  | Total Calls | Status | Issues
--- | --- | --- | --- | --- | --- | --- | ---
Hotel/HotelService.svc/HotelAvail | OK | 14 | 2544 | 200 | 724 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Hotel/HotelService.svc/GetRoomsList | OK | 1 | 1 | 200 | 1694 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Hotel/HotelService.svc/GetHotelDetail | OK | 1 | 27 | 200 | 1694 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Hotel/HotelService.svc/AddHotelReview | OK | 1 | 1 | 200 | 720 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Common/StaticDataService.svc/GetHotelList | OK | 1 | 1 | 200 | 890 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Hotel/HotelService.svc/GetHotelReview | OK | 1 | 5 | 200 | 1694 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)

<h3><a name="HotelFlight"></a>HotelFlight</h3>

 Name | Message | Wait Time(Sec)  |  Size(KB)  |  Status Code  | Total Calls | Status | Issues
--- | --- | --- | --- | --- | --- | --- | ---
HotelFlight/HotelFlightService.svc/HotelFlightSearch | OK | 14 | 2525 | 200 | 553 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
HotelFlight/HotelFlightService.svc/LoadFlight | OK | 1 | 2 | 200 | 540 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Hotel/HotelService.svc/GetHotelPolicy | OK | 1 | 1 | 200 | 1246 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Hotel/HotelService.svc/HoldSelectedRoom | OK | 1 | 1 | 200 | 1245 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
HotelFlight/HotelFlightService.svc/AirportTransportServicePrice | OK | 1 | 4 | 200 | 491 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)

<h3><a name="Package"></a>Package</h3>

 Name | Message | Wait Time(Sec)  |  Size(KB)  |  Status Code  | Total Calls | Status | Issues
--- | --- | --- | --- | --- | --- | --- | ---
Package/PackageService.svc/SearchXPackage | ok | 900 ms |  | 200 | 0 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Package/PackageService.svc/PurchasePackage | ok | 900 ms |  | 200 | 0 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)

<h3><a name="Insurance"></a>Insurance</h3>

 Name | Message | Wait Time(Sec)  |  Size(KB)  |  Status Code  | Total Calls | Status | Issues
--- | --- | --- | --- | --- | --- | --- | ---
Insurance/InsuranceService.svc/PurchaseInsurance | OK | 1 | 1 | 200 | 433 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Insurance/InsuranceService.svc/ShowInsurance | OK | 5 | 54 | 200 | 433 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)

<h3><a name="StaticPage"></a>StaticPage</h3>

 Name | Message | Wait Time(Sec)  |  Size(KB)  |  Status Code  | Total Calls | Status | Issues
--- | --- | --- | --- | --- | --- | --- | ---
Common/StaticDataService.svc/GetAboutUsWithCulture | OK | 1 | 1 | 200 | 436 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
Common/StaticDataService.svc/GetContactUsWithCuture | OK | 1 | 1 | 200 | 433 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)

<h3><a name="Login"></a>Login</h3>

 Name | Message | Wait Time(Sec)  |  Size(KB)  |  Status Code  | Total Calls | Status | Issues
--- | --- | --- | --- | --- | --- | --- | ---
Common/StaticDataService.svc/Login | ok | 900 ms |  | 200 | 0 | :white_check_mark: | [:arrow_upper_right:](https://gitlab.com/nemati/eli/issues/1)
